package revisao;

import java.util.Scanner;

public class Leitor {
	private Scanner meuScanner = new Scanner(System.in);
	
	public int lerInteiro() {
		try {
			return Integer.parseInt(meuScanner.nextLine());
		} catch (Exception e) {
			return -1;
		}
	}
}
