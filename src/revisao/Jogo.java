package revisao;

import java.util.Scanner;

public class Jogo {
	private Tabuleiro meuTabuleiro = new Tabuleiro();
	private int[] placar = {0, 0};
	private Leitor leitor = new Leitor();
	
	public void jogar() {
		System.out.println("Jogada " + meuTabuleiro.getJogadorAtivo());
		
		int x = leitor.lerInteiro();
		int y = leitor.lerInteiro();
		
		meuTabuleiro.jogar(x, y);
	}
	
	public boolean foiFinalizado() {
		return meuTabuleiro.jogoFoiEncerrado();
	}
}
