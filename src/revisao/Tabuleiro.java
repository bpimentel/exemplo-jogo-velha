package revisao;

public class Tabuleiro {
	private String casas[][] = new String[3][3];
	private String jogadores[] = {"X", "O"};
	private int jogadorAtivo = 0;
	private boolean jogoEncerrado = false;
	private boolean deuVelha = false;
	
	public void jogar(int x, int y) {
		if(x > 2 || y > 2 || x < 0 || y < 0) {
			return;
		}
		
		if(casas[x][y] != null) {
			return;
		}
		
		String jogada = jogadores[jogadorAtivo];
		casas[x][y] = jogada;
		
		if(!verificarFimDeJogo()) {
			trocarJogador();
		}
	}
	
	private void trocarJogador() {
		if(jogadorAtivo == 0) {
			jogadorAtivo = 1;
		}else {
			jogadorAtivo = 0;
		}
	}
	
	public String toString() {
		String resultado = "";
		
		for (int i = 0; i < casas.length; i++) {
			for (int j = 0; j < casas[i].length; j++) {
				resultado += casas[i][j] + "|";
			}
			resultado += "\n";
		}
		
		return resultado;
	}
	
	private boolean verificarFimDeJogo() {
		//linha
		for (int i = 0; i < casas.length; i++) {
			if(casas[i][0] == casas[i][1] && casas[i][1] == casas[i][2] && casas[i][0] != null) {
				jogoEncerrado = true;
				return true;
			}
		}
		
		//coluna
		for (int i = 0; i < casas.length; i++) {
			if(casas[0][i] == casas[1][i] && casas[1][i] == casas[2][i] && casas[0][i] != null) {
				jogoEncerrado = true;
				return true;
			}
		}
		
		//diagonal 1
		if(casas[0][0] == casas[1][1] && casas[1][1] == casas[2][2] && casas[0][0] != null) {
			jogoEncerrado = true;
			return true;
		}
		
		//diagonal 2
		if(casas[0][2] == casas[1][1] && casas[1][1] == casas[2][0] && casas[0][2] != null) {
			jogoEncerrado = true;
			return true;
		}
		
		if(verificarVelha()) {
			deuVelha = true;
			jogoEncerrado = true;
			return true;
		}
		
		return false;
		
	}
	
	public int getJogadorAtivo() {
		return jogadorAtivo;
	}
	
	public boolean jogoFoiEncerrado() {
		return jogoEncerrado;
	}
	
	public boolean houveVitoria() {
		return jogoEncerrado && !deuVelha;
	}
	
	private boolean verificarVelha() {
		for (int i = 0; i < casas.length; i++) {
			for (int j = 0; j < casas[i].length; j++) {
				if(casas[i][j] == null) {
					return false;
				}
			}
		}
		
		return true;
	}
}
